import Layout from "../components/Layout";

const Contact = () => {
  return (
    <Layout>
      <div className="relative">
        <div className="bg-black h-[9vh]"></div>
        <div className="pb-[60px] pt-[105px] flex flex-col justify-center items-center">
          <div className="flex flex-col gap-[20px]">
            <p className="font-italia font-medium text-[62px] text-center leading-[1.2] px-7 lg:px-52 xl:px-96 2xl:px-[28rem]">
              Don’t settle for anything less than exceptional.
            </p>

            <p className="text-[22px] text-[#737373] text-center font-light opacity-60 px-7 lg:px-52 xl:px-96 2xl:px-[28rem]">
              Fill out the form and let us help you create a luxury experience
              that is truly unforgettable.
            </p>
          </div>

          <div className="w-full flex flex-col gap-5 mt-[4rem] px-7 lg:px-52 xl:px-[22rem]">
            <div className="flex gap-[10px]">
              <input
                className="bg-transparent w-full p-[10px] text-base outline-none border-b-[1.5px] border-black"
                placeholder="First name"
              />
              <input
                className="bg-transparent w-full p-[10px] text-base outline-none border-b-[1.5px] border-black"
                placeholder="Last name"
              />
            </div>
            <input
              className="bg-transparent w-full p-[10px] text-base outline-none border-b-[1.5px] border-black"
              placeholder="Subject"
            />
            <textarea
              className="bg-transparent w-full h-[10rem] p-[10px] text-base outline-none border-b-[1.5px] border-black mt-1"
              placeholder="Message"
            />

            <div className="flex justify-end mt-2">
              <button className="bg-[#222] h-[43px] w-[218.06px] text-white text-[18px] font-semibold rounded-[40px]">
                Send message
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Contact;
