import Layout from "../components/Layout";
import { images } from "../images";

const About = () => {
  return (
    <Layout>
      <div className="flex flex-col min-h-[100vh]">
        <div className="h-[560px] w-screen">
          <div className="relative w-full h-full">
            <img
              src={images?.about_bg}
              alt=""
              className="w-full h-full object-cover"
            />

            <div className="absolute flex flex-col justify-center items-center top-0 right-0 bottom-0 left-0 bg-black bg-opacity-50">
              <div className="flex flex-col justify-center items-center">
                <p className="font-italia font-medium text-[46px] text-white text-center 2xl:w-[60vw] px-7 lg:px-24 xl:px-64 2xl:px-0 leading-[1.2]">
                  "THE ART OF LIVING IS THE ART OF BRINGING DREAMS AND REALITY
                  TOGETHER"
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="min-h-[320px] px-7 lg:px-36 xl:px-36 2xl:px-[4.4rem] py-[75px] bg-[#232222] text-white -mt-2">
          <p className="font-italia text-[32px] md:text-[52px] mb-[16px]">
            Our network
          </p>

          <p className="max-w-[720px] text-[20px] font-light leading-[1.6] text-[#f9f9f9] mb-[15px]">
            We’re proud to offer our luxury concierge services in some of the
            world’s most vibrant and exciting cities. From New York to Paris,
            Marbella to Dubai, our team of experienced concierge profession- als
            is here to provide you with the highest level of service and
            attention to detail.
          </p>
          <p className="max-w-[720px] text-[20px] font-light leading-[1.6] text-[#f9f9f9]">
            No matter where your travels take you, our luxury concierge ser-
            vice is here to help you feel at home. From transportation to
            accommodations, dining reservations to event planning, we take care
            of everything so that you can focus on enjoying your trip.
          </p>
        </div>
      </div>
    </Layout>
  );
};

export default About;
