import { FC } from "react";
import { useNavigate } from "react-router-dom";
import { data } from "../assets/data/serviceDetails";
import Layout from "../components/Layout";
import BackIcon from "../components/utils/icons/BackIcon";

const ServiceDetails: FC = () => {
  const navigate = useNavigate();
  const urlParts = window.location.pathname.split("/");

  const selectedText = urlParts[2];

  const pageData = data.find((service) => service.title === selectedText);

  return (
    <Layout>
      <div className="xl:h-screen w-screen flex flex-col">
        <div className="bg-black h-[10vh]"></div>
        <div className="flex-1 w-full grid grid-cols-1 xl:grid-cols-2 h-full">
          <div className="w-full">
            <img
              src={pageData?.image}
              alt=""
              className="max-h-[65vh] xl:max-h-[90vh] w-full object-cover"
            />
          </div>
          <div className="w-full flex flex-col gap-14 py-10 px-7 md:px-20">
            <div
              onClick={() => navigate(-1)}
              className="cursor-pointer flex items-center gap-2"
            >
              <BackIcon />
              <p className="tracking-tighter font-bold uppercase">Back</p>
            </div>

            <div className="flex flex-col gap-5">
              <p className="font-italia text-[42px] font-[900] uppercase -mb-2">
                {pageData?.title}
              </p>

              <p className="font-light text-[18px]">{pageData?.description}</p>

              <p className="font-light text-[18px]">{pageData?.sub}</p>

              <button className="h-[43px] w-[179px] rounded-[40px] text-white font-semibold outline-none bg-[#C1B686] mt-6">
                Book now
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ServiceDetails;
