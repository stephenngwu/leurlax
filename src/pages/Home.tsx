import { FC } from "react";
import Layout from "../components/Layout";
import { images } from "../images";

const Home: FC = () => {
  return (
    <Layout>
      <div className="h-screen w-screen animate-fade">
        <div className="relative w-full h-full">
          <img
            src={images?.bg_img}
            alt=""
            className="w-full h-full object-cover"
          />
          <div className="absolute flex flex-col justify-center items-center top-0 right-0 bottom-0 left-0 bg-black bg-opacity-50">
            <div className="flex flex-col justify-center items-center -mt-4">
              <p className="font-italia font-medium text-[30px] sm:text-[72px] text-[#f2f5dc] text-center -mb-1">
                YOUR ONE STOP
              </p>
              <p className="font-italia font-medium text-[30px] sm:text-[72px] text-[#c1b686] text-center md:-mt-2">
                LUXURY CONCIERGE
              </p>

              <button className="bg-[#f2f5dc] mt-7 uppercase text-[18px] py-[8px] h-[43px] w-[192.88px] rounded-[40px] font-semibold outline-none shadow-xl tracking-tighter">
                book now
              </button>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default Home;
