import { FC } from "react";
import { useNavigate } from "react-router-dom";
import Layout from "../components/Layout";
import { images } from "../images";

const services = [
  {
    title: "Rentals",
    image: images?.img_one,
    list: ["Private jets", "Yatchs", "Super cars"],
  },
  {
    title: "Transfers",
    image: images?.img_two,
    list: ["From/To Airport", "Private chauffer"],
  },
  {
    title: "Property",
    image: images?.img_three,
    list: ["Villas for rent", "Apartment for rent"],
  },
  {
    title: "Lifestyle",
    image: images?.img_four,
    list: ["Events/Beach & night clubs", "Restraurants", "Personal training"],
  },
];

const Services: FC = () => {
  const navigate = useNavigate();

  return (
    <Layout>
      <div className="h-full lg:h-screen w-screen">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:flex lg:flex-row justify-start h-full w-full">
          {services?.map((service, index) => (
            <div
              key={index}
              onClick={() =>
                navigate(`/services/${service?.title?.toLowerCase()}`)
              }
              className="relative h-[50vh] lg:h-full w-full lg:w-[25%] cursor-pointer"
            >
              <img
                src={service?.image}
                alt=""
                className="h-full w-full object-cover bg-black bg-opacity-90"
              />

              <div className="absolute bottom-0 lg:top-[70%] left-0 right-0 flex flex-col px-6 lg:px-16 py-10 gap-4 z-10">
                <p className="font-italia text-3xl lg:text-[32px] text-white mb-4 -ml-1">
                  {service?.title}
                </p>
                {service?.list?.map((item, idx) => (
                  <p
                    key={idx}
                    className="text-[#FFFFFFCC] font-light -mt-2 -ml-1 leading-6"
                  >
                    {item}
                  </p>
                ))}
              </div>

              <div className="absolute top-0 right-0 bottom-0 left-0 bg-black bg-opacity-70 hover:bg-opacity-40 transition-all"></div>
            </div>
          ))}
        </div>
      </div>
    </Layout>
  );
};

export default Services;
