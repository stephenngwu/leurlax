import { images } from "../../images";

export const data = [
  {
    title: "rentals",
    description:
      "Our luxury transfer service provides a seamless and sophisticated transportation experience, designed to meet the needs of the most discerning travelers. From the moment you arrive, our team of experienced professionals will ensure that every detail of your transfer is executed flawlessly, from the comfort of our luxury vehicles to the personalized amenities and services we offer.",
    sub: "Whether you’re ar- riving at the airport, departing for a special event, or simply need to get around town in style, our transfer service is designed to exceed your expectations and provide an unparalleled level of comfort, convenience, and luxury.",
    image: images?.img_one,
  },
  {
    title: "transfers",
    description:
      "Our luxury transfer service provides a seamless and sophisticated transportation experience, designed to meet the needs of the most discerning travelers. From the moment you arrive, our team of ex- perienced professionals will ensure that every detail of your transfer is executed flawlessly, from the comfort of our luxury vehicles to the personalized amenities and services we offer",
    sub: "Whether you’re ar- riving at the airport, departing for a special event, or simply need to get around town in style, our transfer service is designed to exceed your expectations and provide an unparalleled level of comfort, convenience, and luxury.",
    image: images?.img_two,
  },
  {
    title: "property",
    description:
      "We are passionate about creating unforgettable experiences for those who appreciate the finer things in life. Our bespoke range of lifestyle services is designed to elevate your every moment, from securing coveted reservations at the city’s most exclusive restaurants, to gaining access to the most sought-after nightclubs and events, and even working with a personal trainer to sculpt your physique to perfection.",
    sub: "With our unparalleled insider knowledge and extensive network of contacts, we are able to deliver the very best of what Marbella has to offer, tailored to your every need and desire. Our attentive team is dedicated to ensuring that every detail is taken care of, leaving you free to simply sit back and enjoy the ultimate in luxurious living.",
    image: images?.img_three,
  },
  {
    title: "lifestyle",
    description:
      "We are passionate about creating unforgettable experiences for those who appreciate the finer things in life. Our bespoke range of lifestyle services is designed to elevate your every moment, from securing coveted reservations at the city’s most exclusive restaurants, to gaining access to the most sought-after nightclubs and events, and even working with a personal trainer to sculpt your physique to perfection.",
    sub: "With our unparalleled insider knowledge and extensive network of contacts, we are able to deliver the very best of what Marbella has to offer, tailored to your every need and desire. Our attentive team is dedicated to ensuring that every detail is taken care of, leaving you free to simply sit back and enjoy the ultimate in luxurious living.",
    image: images?.img_four,
  },
];
