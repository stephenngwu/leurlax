import about_bg from "./assets/images/about_bg.svg";
import bg_img from "./assets/images/home_bg.png";
import logo from "./assets/images/logo.svg";
import img_four from "./assets/images/service_img_four.svg";
import img_one from "./assets/images/service_img_one.svg";
import img_three from "./assets/images/service_img_three.svg";
import img_two from "./assets/images/service_img_two.svg";

export const images = {
  bg_img,
  img_four,
  img_one,
  img_three,
  img_two,
  about_bg,
  logo,
};
