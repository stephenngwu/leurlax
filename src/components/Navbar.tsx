import { useState } from "react";
import { useNavigate } from "react-router-dom";
import DropdownIcon from "../components/utils/icons/DropdownIcon";
import GlobeIcon from "../components/utils/icons/GlobeIcon";
import { images } from "../images";
import Dropdown from "./Dropdown";
import NavbarIcon from "./utils/icons/NavbarIcon";

const Navbar = () => {
  const navigate = useNavigate();
  const [isMobile, setIsMobile] = useState(false);
  const urlParts = window.location.pathname.split("/");
  const isContactPage = urlParts[1] === "contact";

  return (
    <div
      className={
        isContactPage
          ? "w-full flex justify-between items-center py-3 px-7 lg:px-20 xl:px-36 2xl:px-[4.5rem] bg-black fixed z-40 top-0 left-0 h-[80px]"
          : "w-full flex justify-between items-center py-3 px-7 lg:px-20 xl:px-36 2xl:px-[4.5rem] bg-transparent fixed z-40 top-0 left-0 h-[80px]"
      }
    >
      <div onClick={() => navigate("/")} className="cursor-pointer">
        <img src={images?.logo} alt="" />
      </div>

      <div className="hidden lg:flex justify-end items-center gap-10 mr-3">
        <p
          onClick={() => navigate("/")}
          className="text-[18px] font-medium hover:border-b border-white text-white cursor-pointer transition-all leading-7"
        >
          Home
        </p>
        <p
          onClick={() => navigate("/services")}
          className="text-[18px] font-medium hover:border-b border-white text-white cursor-pointer transition-all leading-7"
        >
          Services
        </p>
        <p
          onClick={() => navigate("/about")}
          className="text-[18px] font-medium hover:border-b border-white text-white cursor-pointer transition-all
          leading-7"
        >
          About
        </p>
        <p
          onClick={() => navigate("/contact")}
          className="text-[18px] font-medium hover:border-b border-white text-white cursor-pointer transition-all leading-7"
        >
          Contact
        </p>

        <div className="flex items-center gap-2 cursor-pointer">
          <GlobeIcon />
          <DropdownIcon />
        </div>
      </div>

      <div className="ml-10 inline-block lg:hidden cursor-pointer">
        <div onClick={() => setIsMobile(true)} className="">
          <NavbarIcon />
        </div>
        {isMobile ? <Dropdown setIsMobile={setIsMobile} /> : null}
      </div>
    </div>
  );
};

export default Navbar;
