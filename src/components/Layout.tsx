import { ReactNode } from "react";
import Navbar from "./Navbar";

interface MyComponentProps {
  children: ReactNode;
}

const Layout: React.FC<MyComponentProps> = ({ children }) => {
  return (
    <main className="w-full overflow-hidden">
      <Navbar />
      {children}
    </main>
  );
};

export default Layout;
