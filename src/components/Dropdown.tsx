import { useNavigate } from "react-router-dom";
import { images } from "../images";
import CancelIcon from "./utils/icons/CancelIcon";

interface MyComponentProps {
  setIsMobile: React.Dispatch<React.SetStateAction<boolean>>;
}

const Dropdown: React.FC<MyComponentProps> = ({ setIsMobile }) => {
  const navigate = useNavigate();
  return (
    <div className="fixed top-0 left-0 h-screen w-screen bg-[#222222] flex flex-col">
      <div className="flex justify-between items-center w-full py-5 px-10">
        <div onClick={() => navigate("/")} className="cursor-pointer">
          <img src={images?.logo} alt="" />
        </div>
        <div onClick={() => setIsMobile(false)} className="cursor-pointer z-10">
          <CancelIcon />
        </div>
      </div>
      <div className="flex-1 flex flex-col justify-center items-center h-full w-full gap-10 -mt-12">
        <p
          onClick={() => {
            setIsMobile(false);
            navigate("/");
          }}
          className="text-white font-bold text-[42px]"
        >
          Home
        </p>
        <p
          onClick={() => {
            setIsMobile(false);
            navigate("/services");
          }}
          className="text-white font-bold text-[42px]"
        >
          Services
        </p>
        <p
          onClick={() => {
            setIsMobile(false);
            navigate("/about");
          }}
          className="text-white font-bold text-[42px]"
        >
          About
        </p>
        <p
          onClick={() => {
            setIsMobile(false);
            navigate("/contact");
          }}
          className="text-white font-bold text-[42px]"
        >
          Contact
        </p>
      </div>
    </div>
  );
};

export default Dropdown;
